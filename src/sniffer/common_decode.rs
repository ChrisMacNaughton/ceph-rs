extern crate nom;
extern crate uuid;

use self::nom::{le_u8, le_u16, le_u32, le_i64, le_u64};
use self::nom::IResult::Done;
use sniffer::serial::*;
use std::net::Ipv4Addr;

macro_rules! cond_with_error(
  ($i:expr, $cond:expr, $submac:ident!( $($args:tt)* )) => (
    {
      if $cond {
        match $submac!($i, $($args)*) {
          nom::IResult::Done(i,o)     => nom::IResult::Done(i, ::std::option::Option::Some(o)),
          nom::IResult::Error(e)      => nom::IResult::Error(e),
          nom::IResult::Incomplete(i) => nom::IResult::Incomplete(i)
        }
      } else {
        nom::IResult::Done($i, ::std::option::Option::None)
      }
    }
  );
  ($i:expr, $cond:expr, $f:expr) => (
    cond!($i, $cond, call!($f));
  );
);

#[test]
fn test_ceph_read_entity_name_t() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = EntityNameT::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_EntityNameT() {
    //let bytes = vec![];
    //let result = EntityNameT::write_to_wire();
    //println!("ceph_write_EntityNameT{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct EntityNameT {
    pub _type: u8,
    pub _num: i64,
}

impl<'a> CephPrimitiveVersioned<'a> for EntityNameT {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
    		_type: le_u8 ~
    		_num: le_i64,
    		||{
    			EntityNameT{
    			_type: _type,
    			_num: _num,
    		}
    	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}

#[test]
fn test_ceph_read_ceph_sockaddr_storage() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = CephSockaddrStorage::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_CephSockaddrStorage() {
    //let bytes = vec![];
    //let result = CephSockaddrStorage::write_to_wire();
    //println!("ceph_write_CephSockaddrStorage{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct CephSockaddrStorage {
    pub ss_family: u16,
}

impl<'a> CephPrimitiveVersioned<'a> for CephSockaddrStorage {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		ss_family: le_u16,
		||{
			CephSockaddrStorage{
			ss_family: ss_family,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}

#[test]
fn test_ceph_read_entity_inst_t() {
    let bytes = vec![
        4, //OSD
        2, 0, 0, 0, 0, 0, 0, 0, //OSD number
        0, 0, 0, 0, //EntityAddr
        201, 67, 0, 0,
        0, 2,
        26, 144,
        172, 31, 24, 179,
        //Padding crap
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];
    let x: &[u8] = &[];
    let expected_result = EntityInstT {
        name: EntityNameT { _type: 4, _num: 2 },
        addr: EntityAddr {
            port: 6800,
            nonce: 17353,
            addr: Addr::V4addr(Ipv4Addr::new(172,31,24,179))
        }
    };
    let result = EntityInstT::read_from_wire(&bytes, 0, 0);
    println!("EntityInstT: {:?}", result);
    assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_EntityInstT() {
    //let bytes = vec![];
    //let result = EntityInstT::write_to_wire();
    //println!("ceph_write_EntityInstT{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct EntityInstT {
    pub name: EntityNameT,
    pub addr: EntityAddr,
}

impl<'a> CephPrimitiveVersioned<'a> for EntityInstT {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		name: dbg!(call!(EntityNameT::read_from_wire, head_version, compat_version)) ~
		addr: dbg!(call!(EntityAddr::read_from_wire, head_version, compat_version)),
		||{
			EntityInstT{
			name: name,
			addr: addr,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_write_EversionT() {
    //let bytes = vec![];
    //let result = EversionT::write_to_wire();
    //println!("ceph_write_EversionT{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct EversionT {
    pub version: u64,
    pub epoch: u32,
}

impl<'a> CephPrimitiveVersioned<'a> for EversionT {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		version: le_u64 ~
		epoch: le_u32,
		||{
			EversionT{
			version: version,
			epoch: epoch,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
