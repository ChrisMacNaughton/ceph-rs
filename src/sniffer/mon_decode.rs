extern crate nom;
extern crate num;
extern crate uuid;

use sniffer::serial;
use self::num::FromPrimitive;
use self::nom::{le_u8, le_i16, le_u16, le_i32, le_u32, le_u64, be_f64};
use self::nom::IResult::Done;
use self::uuid::{Uuid};
use std::u16;
use std::net::Ipv4Addr;
use sniffer::common_decode::{EntityNameT, EntityInstT};
use sniffer::serial::*;
use rustc_serialize::json;

macro_rules! cond_with_error(
  ($i:expr, $cond:expr, $submac:ident!( $($args:tt)* )) => (
    {
      if $cond {
        match $submac!($i, $($args)*) {
          nom::IResult::Done(i,o)     => nom::IResult::Done(i, ::std::option::Option::Some(o)),
          nom::IResult::Error(e)      => nom::IResult::Error(e),
          nom::IResult::Incomplete(i) => nom::IResult::Incomplete(i)
        }
      } else {
        nom::IResult::Done($i, ::std::option::Option::None)
      }
    }
  );
  ($i:expr, $cond:expr, $f:expr) => (
    cond!($i, $cond, call!($f));
  );
);

enum_from_primitive!{
#[repr(u8)]
#[derive(RustcEncodable,Debug, Clone,Eq,PartialEq)]
pub enum OpTypeT{
    OpScrub = 1,         // leader->peon: scrub (a range of) keys
    OpResult = 2,        // peon->leader: result of a scrub
}
}

#[test]
fn test_ceph_read_mlog() {
    ////let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mlog::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    ////assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_mlog() {
    //let bytes = vec![];
    //let result = Mlog::write_to_wire();
    //println!("ceph_write_Mlog{:?}", result);
    // assert_eq!(result, expected_bytes);
}

enum_from_primitive!{
#[repr(u16)]
#[derive(RustcEncodable,Debug, Clone,Eq,PartialEq)]
pub enum CLogType {
  Debug = 0,
  Info = 1,
  Sec = 2,
  Warn = 3,
  Error = 4,
  Unknown = u16::MAX,
}
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct LogEntry<'a> {
    pub who: EntityInstT,
    pub stamp: Utime,
    pub seq: u64,
    pub prio: CLogType,
    pub msg: &'a str,
    pub channel: &'a str,
}

impl<'a> CephPrimitiveVersioned<'a> for LogEntry<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
          who: call!(EntityInstT::read_from_wire, head_version, compat_version)~
          stamp: call!(Utime::read_from_wire, head_version, compat_version) ~
          seq: le_u64 ~
          prio_bits: le_u16 ~
          prio: expr_opt!(CLogType::from_u16(prio_bits)) ~
          msg: parse_str ~
          channel: parse_str,
		||{
			LogEntry{
              who: who,
              stamp: stamp,
              seq: seq,
              prio: prio,
              msg: msg,
              channel: channel,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mlog<'a> {
    pub fsid: Uuid,
    pub entries: Vec<LogEntry<'a>>,
}

impl<'a> CephPrimitiveVersioned<'a> for Mlog<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		fsid: parse_fsid ~
        count: le_u32 ~
		entries: count!(call!(LogEntry::read_from_wire, head_version, compat_version), count as usize),
		||{
			Mlog{
			fsid: fsid,
			entries: entries,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_mmongetversionreply() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmongetversionreply::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_mmongetversionreply() {
    //let bytes = vec![];
    //let result = Mmongetversionreply::write_to_wire();
    //println!("ceph_write_Mmongetversionreply{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mmongetversionreply {
    pub handle: u64,
    pub version: u64,
    pub oldest_version: u64,
}

impl<'a> CephPrimitiveVersioned<'a> for Mmongetversionreply {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		handle: le_u64 ~
		version: le_u64 ~
		oldest_version: le_u64,
		||{
			Mmongetversionreply{
			handle: handle,
			version: version,
			oldest_version: oldest_version,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_paxosservicemessage() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Paxosservicemessage::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_paxosservicemessage() {
    //let bytes = vec![];
    //let result = Paxosservicemessage::write_to_wire();
    //println!("ceph_write_Paxosservicemessage{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Paxosservicemessage {
    pub version: u64,
    pub deprecated_session_mon: i16,
    pub deprecated_session_mon_tid: u64,
    pub rx_election_epoch: u32,
}

impl<'a> CephPrimitiveVersioned<'a> for Paxosservicemessage {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		version: le_u64 ~
		deprecated_session_mon: le_i16 ~
		deprecated_session_mon_tid: le_u64 ~
		rx_election_epoch: le_u32,
		||{
			Paxosservicemessage{
			version: version,
			deprecated_session_mon: deprecated_session_mon,
			deprecated_session_mon_tid: deprecated_session_mon_tid,
			rx_election_epoch: rx_election_epoch,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_mcommand() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mcommand::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_mcommand() {
    //let bytes = vec![];
    //let result = Mcommand::write_to_wire();
    //println!("ceph_write_Mcommand{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mcommand<'a> {
    pub fsid: Uuid,
    pub cmd: Vec<&'a str>,
}

impl<'a> CephPrimitiveVersioned<'a> for Mcommand<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		fsid: parse_fsid ~
		count: le_u32~
		cmd: count!(parse_str, count as usize),
		||{
			Mcommand{
			fsid: fsid,
			cmd: cmd,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_mforward() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mforward::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_mforward() {
    //let bytes = vec![];
    //let result = Mforward::write_to_wire();
    //println!("ceph_write_Mforward{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct StringConstraint<'a> {
    pub value: &'a str,
    pub prefix: &'a str,
}

impl<'a> CephPrimitiveVersioned<'a> for StringConstraint<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
          value: parse_str ~
          prefix: parse_str,
		||{
			StringConstraint{
                value: value,
                prefix: prefix,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
bitflags!{
    #[derive(RustcEncodable)]
    flags MonCapFlags: u8 {
        const MON_CAP_R     = (1 << 1),      // read
        const MON_CAP_W     = (1 << 2),      // write
        const MON_CAP_X     = (1 << 3),      // execute
        const MON_CAP_ALL   =
            MON_CAP_R.bits |
            MON_CAP_W.bits |
            MON_CAP_X.bits,
        const MON_CAP_ANY   = 0xff,          // *
    }
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct MonCapGrant<'a> {
    //
    // A grant can come in one of four forms:
    //
    //  - a blanket allow ('allow rw', 'allow *')
    //    - this will match against any service and the read/write/exec flags
    //      in the mon code.  semantics of what X means are somewhat ad hoc.
    //
    //  - a service allow ('allow service mds rw')
    //    - this will match against a specific service and the r/w/x flags.
    //
    //  - a profile ('allow profile osd')
    //    - this will match against specific monitor-enforced semantics of what
    //      this type of user should need to do.  examples include 'osd', 'mds',
    //      'bootstrap-osd'.
    //
    //  - a command ('allow command foo', 'allow command bar with arg1=val1 arg2 prefix val2')
    //      this includes the command name (the prefix string), and a set
    //      of key/value pairs that constrain use of that command.  if no pairs
    //      are specified, any arguments are allowed; if a pair is specified, that
    //      argument must be present and equal or match a prefix.
    //
    pub service: &'a str,
    pub profile: &'a str,
    pub command: &'a str,
    pub command_args: Vec<(&'a str, StringConstraint<'a>)>,
    pub flags: MonCapFlags,
}

impl<'a> CephPrimitiveVersioned<'a> for MonCapGrant<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
          service: parse_str ~
          profile: parse_str ~
          command: parse_str ~
          arg_length: le_u32 ~
          command_args: count!(
              pair!(parse_str, call!(StringConstraint::read_from_wire, head_version, compat_version)),
              arg_length as usize) ~
        flag_bits: le_u8 ~
        flags: expr_opt!(MonCapFlags::from_bits(flag_bits)),
		||{
			MonCapGrant{
                service: service,
                profile: profile,
                command: command,
                command_args: command_args,
                flags: flags,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct MonCap<'a> {
    pub text: &'a str,
    pub grants: Vec<MonCapGrant<'a>>,
}

impl<'a> CephPrimitiveVersioned<'a> for MonCap<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		text: parse_str ~
        grant_length: le_u32 ~
        grants: count!(call!(MonCapGrant::read_from_wire, head_version, compat_version), grant_length as usize),
		||{
			MonCap{
                text: text,
                grants: grants,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mforward<'a> {
    pub tid: u64,
    pub client: EntityInstT,
    pub client_caps: MonCap<'a>,
    pub con_features: u64,
    pub entity_name: EntityNameT,
    pub msg: Paxosservicemessage,
    pub msg_bl: &'a [u8],
}

impl<'a> CephPrimitiveVersioned<'a> for Mforward<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        let head_version = 1;
        let compat_version = 1;
        chain!(input,
		tid: le_u64 ~
		client: call!(EntityInstT::read_from_wire, head_version, compat_version) ~
		client_caps: call!(MonCap::read_from_wire, head_version, compat_version) ~
		con_features: le_u64 ~
		entity_name: call!(EntityNameT::read_from_wire, head_version, compat_version) ~
		msg: call!(Paxosservicemessage::read_from_wire, head_version, compat_version) ~
        msg_size: le_u32 ~
		msg_bl: take!(msg_size),
		||{
			Mforward{
			tid: tid,
			client: client,
			client_caps: client_caps,
			con_features: con_features,
			entity_name: entity_name,
			msg: msg,
			msg_bl: msg_bl,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_mmonglobalid() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmonglobalid::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_mmonglobalid() {
    //let bytes = vec![];
    //let result = Mmonglobalid::write_to_wire();
    //println!("ceph_write_Mmonglobalid{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mmonglobalid {
    pub old_max_id: u64,
}

impl<'a> CephPrimitiveVersioned<'a> for Mmonglobalid {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		old_max_id: le_u64,
		||{
			Mmonglobalid{
			old_max_id: old_max_id,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_mmongetversion() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmongetversion::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_mmongetversion() {
    //let bytes = vec![];
    //let result = Mmongetversion::write_to_wire();
    //println!("ceph_write_Mmongetversion{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mmongetversion<'a> {
    pub handle: u64,
    pub what: &'a str,
}

impl<'a> CephPrimitiveVersioned<'a> for Mmongetversion<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		handle: le_u64 ~
		what: parse_str,
		||{
			Mmongetversion{
			handle: handle,
			what: what,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_MMonCommand() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmoncommand::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_Mmoncommand() {
    //let bytes = vec![];
    //let result = Mmoncommand::write_to_wire();
    //println!("ceph_write_Mmoncommand{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mmoncommand<'a> {
    pub fsid: Uuid,
    pub cmd: Vec<&'a str>,
}

impl<'a> CephPrimitiveVersioned<'a> for Mmoncommand<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		fsid: parse_fsid ~
		count: le_u32 ~
		cmd: count!(parse_str, count as usize),
		||{
			Mmoncommand{
			fsid: fsid,
			cmd: cmd,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_MMonSubscribeAck() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmonsubscribeack::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_Mmonsubscribeack() {
    //let bytes = vec![];
    //let result = Mmonsubscribeack::write_to_wire();
    //println!("ceph_write_Mmonsubscribeack{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mmonsubscribeack {
    pub interval: u32,
    pub fsid: Uuid,
}

impl<'a> CephPrimitiveVersioned<'a> for Mmonsubscribeack {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		interval: le_u32 ~
		fsid: parse_fsid,
		||{
			Mmonsubscribeack{
			interval: interval,
			fsid: fsid,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_MMonQuorumService() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmonquorumservice::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_Mmonquorumservice() {
    //let bytes = vec![];
    //let result = Mmonquorumservice::write_to_wire();
    //println!("ceph_write_Mmonquorumservice{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mmonquorumservice {
    pub epoch: u32,
    pub round: u64,
}

impl<'a> CephPrimitiveVersioned<'a> for Mmonquorumservice {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		epoch: le_u32 ~
		round: le_u64,
		||{
			Mmonquorumservice{
			epoch: epoch,
			round: round,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_MMonScrub() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmonscrub::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_Mmonscrub() {
    //let bytes = vec![];
    //let result = Mmonscrub::write_to_wire();
    //println!("ceph_write_Mmonscrub{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct ScrubResult<'a> {
    pub prefix_crc: Vec<(&'a str, u32)>, // < prefix -> crc
    pub prefix_keys: Vec<(&'a str, u64)>, // < prefix -> key count
}

impl<'a> CephPrimitiveVersioned<'a> for ScrubResult<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		num_crc: le_u32 ~
        prefix_crc: count!(pair!(parse_str, le_u32), num_crc as usize) ~
        num_keys: le_u32~
        prefix_keys: count!(pair!(parse_str, le_u64), num_keys as usize),
		||{
			ScrubResult{
                prefix_crc: prefix_crc,
                prefix_keys: prefix_keys,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mmonscrub<'a> {
    pub op: OpTypeT,
    pub version: u64,
    pub result: ScrubResult<'a>,
    pub num_keys: i32,
    pub key: (&'a str, &'a str),
}

impl<'a> CephPrimitiveVersioned<'a> for Mmonscrub<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        let head_version = 2;
        let compat_version = 1;
        chain!(input,
        op_bits: le_u8~
		op: expr_opt!(OpTypeT::from_u8(op_bits)) ~
		version: le_u64 ~
		result: call!(ScrubResult::read_from_wire, head_version, compat_version) ~
		num_keys: le_i32 ~
		key: pair!(parse_str, parse_str),
		||{
			Mmonscrub{
			op: op,
			version: version,
			result: result,
			num_keys: num_keys,
			key: key,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_MMonCommandAck() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmoncommandack::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_Mmoncommandack() {
    //let bytes = vec![];
    //let result = Mmoncommandack::write_to_wire();
    //println!("ceph_write_Mmoncommandack{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mmoncommandack<'a> {
    pub cmd: Vec<&'a str>,
    pub r: i32,
    pub rs: &'a str,
}

impl<'a> CephPrimitiveVersioned<'a> for Mmoncommandack<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		count: le_u32 ~
		cmd: count!(parse_str, count as usize)~
		r: le_i32 ~
		rs: parse_str,
		||{
			Mmoncommandack{
			cmd: cmd,
			r: r,
			rs: rs,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}

#[test]
fn test_ceph_read_MMonMap() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmonmap::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_Mmonmap() {
    //let bytes = vec![];
    //let result = Mmonmap::write_to_wire();
    //println!("ceph_write_Mmonmap{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mmonmap<'a> {
    pub monmapbl: &'a [u8],
}

impl<'a> CephPrimitiveVersioned<'a> for Mmonmap<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
        monmap_size: le_u32 ~
		monmapbl: take!(monmap_size),
		||{
			Mmonmap{
			monmapbl: monmapbl,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_MAuthReply() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mauthreply::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_Mauthreply() {
    //let bytes = vec![];
    //let result = Mauthreply::write_to_wire();
    //println!("ceph_write_Mauthreply{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mauthreply<'a> {
    pub protocol: u32,
    pub result: i32,
    pub global_id: u64,
    pub result_msg: &'a str,
    pub result_bl: &'a [u8],
}

impl<'a> CephPrimitiveVersioned<'a> for Mauthreply<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		protocol: le_u32 ~
		result: le_i32 ~
		global_id: le_u64 ~
		result_msg: parse_str ~
        result_bl_size: le_u32 ~
		result_bl: take!(result_bl_size),
		||{
			Mauthreply{
			protocol: protocol,
			result: result,
			global_id: global_id,
			result_msg: result_msg,
			result_bl: result_bl,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_mtimecheck() {
    let bytes = vec![1, 0, 0, 0, 50, 0, 0, 0, 0, 0, 0, 0, 63, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let x: &[u8] = &[];
    let expected_result = MTimeCheck{
        op: TimeCheckOp::Ping,
        epoch: 50,
        round: 1343,
        timestamp: Utime { tv_sec: 0, tv_nsec: 0 },
        skews: vec![],
        latencies: vec![]
    };
    let result = MTimeCheck::read_from_wire(&bytes, 0, 0);
    println!("MTimeCheck: {:?}", result);
    assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_mtimecheck() {
    //let bytes = vec![];
    //let result = Mtimecheck::write_to_wire();
    //println!("ceph_write_Mtimecheck{:?}", result);
    // assert_eq!(result, expected_bytes);
}

enum_from_primitive!{
#[repr(i32)]
#[derive(RustcEncodable,Debug, Clone,Eq,PartialEq)]
pub enum TimeCheckOp{
    Ping = 1,
    Pong = 2,
    Report = 3,
}
}

#[derive(RustcEncodable,Debug,PartialEq)]
pub struct MTimeCheck {
    pub op: TimeCheckOp,
    pub epoch: u64,
    pub round: u64,
    pub timestamp: Utime,
    pub skews: Vec<(EntityInstT, f64)>,
    pub latencies: Vec<(EntityInstT, f64)>,
}

impl<'a> CephPrimitiveVersioned<'a> for MTimeCheck {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        let head_version = 1;
        chain!(input,
        op_bits: le_i32 ~
        op: expr_opt!(TimeCheckOp::from_i32(op_bits)) ~
		epoch: le_u64 ~
		round: le_u64 ~
		timestamp: call!(Utime::read_from_wire, head_version, compat_version) ~
		count: le_u32 ~
		skews: count!(
            pair!(call!(EntityInstT::read_from_wire, head_version, compat_version),be_f64), count as usize) ~
		count: le_u32 ~
		latencies: count!(
            pair!(call!(EntityInstT::read_from_wire, head_version, compat_version),be_f64), count as usize),
		||{
			MTimeCheck{
			op: op,
			epoch: epoch,
			round: round,
			timestamp: timestamp,
			skews: skews,
			latencies: latencies,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_MMonElection() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmonelection::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_Mmonelection() {
    //let bytes = vec![];
    //let result = Mmonelection::write_to_wire();
    //println!("ceph_write_Mmonelection{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mmonelection<'a> {
    pub fsid: Uuid,
    pub op: i32,
    pub epoch: u32,
    pub monmap_bl: &'a [u8],
    pub quorum: Vec<i32>,
    pub quorum_features: u64,
    pub sharing_bl: &'a [u8],
    pub defunct_one: u64,
    pub defunct_two: u64,
}

impl<'a> CephPrimitiveVersioned<'a> for Mmonelection<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        let propose = 1;
        let ack = 1;
        let nak = 1;
        let victory = 1;
        chain!(input,
		fsid: parse_fsid ~
		op: le_i32 ~
		epoch: le_u32 ~
        monmap_size: le_u32 ~
		monmap_bl: take!(monmap_size) ~
		count: le_u32 ~
		quorum: count!(le_i32,count as usize) ~
		quorum_features: le_u64 ~
        sharing_size: le_u32 ~
		sharing_bl: take!(sharing_size) ~
		defunct_one: le_u64 ~
		defunct_two: le_u64,
		||{
			Mmonelection{
			fsid: fsid,
			op: op,
			epoch: epoch,
			monmap_bl: monmap_bl,
			quorum: quorum,
			quorum_features: quorum_features,
			sharing_bl: sharing_bl,
			defunct_one: defunct_one,
			defunct_two: defunct_two,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}

#[test]
fn test_ceph_read_MMonProbe() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmonprobe::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_Mmonprobe() {
    //let bytes = vec![];
    //let result = Mmonprobe::write_to_wire();
    //println!("ceph_write_Mmonprobe{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mmonprobe<'a> {
    pub fsid: Uuid,
    pub op: i32,
    pub name: &'a str,
    pub quorum: Vec<i32>,
    pub monmap_bl: &'a [u8],
    pub paxos_first_version: u64,
    pub paxos_last_version: u64,
    pub has_ever_joined: u8,
    pub required_features: u64,
}

impl<'a> CephPrimitiveVersioned<'a> for Mmonprobe<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        let head_version = 1;
        let compat_version = 1;
        chain!(input,
		fsid: parse_fsid ~
		op: le_i32 ~
		name: parse_str ~
		count: le_u32 ~
		quorum: count!(le_i32,count as usize) ~
        monmap_size: le_u32 ~
		monmap_bl: take!(monmap_size) ~
		paxos_first_version: le_u64 ~
		paxos_last_version: le_u64 ~
		has_ever_joined: le_u8 ~
		required_features: le_u64,
		||{
			Mmonprobe{
			fsid: fsid,
			op: op,
			name: name,
			quorum: quorum,
			monmap_bl: monmap_bl,
			paxos_first_version: paxos_first_version,
			paxos_last_version: paxos_last_version,
			has_ever_joined: has_ever_joined,
			required_features: required_features,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_MMonMetadata() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmonmetadata::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_Mmonmetadata() {
    //let bytes = vec![];
    //let result = Mmonmetadata::write_to_wire();
    //println!("ceph_write_Mmonmetadata{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mmonmetadata<'a> {
    pub data: Vec<(&'a str, &'a str)>,
}

impl<'a> CephPrimitiveVersioned<'a> for Mmonmetadata<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
        data_size: le_u32 ~
		data: count!(pair!(parse_str, parse_str), data_size as usize),
		||{
			Mmonmetadata{
			data: data,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}

#[test]
fn test_ceph_read_MMonJoin() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmonjoin::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_Mmonjoin() {
    //let bytes = vec![];
    //let result = Mmonjoin::write_to_wire();
    //println!("ceph_write_Mmonjoin{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mmonjoin<'a> {
    pub fsid: Uuid,
    pub name: &'a str,
    pub addr: EntityAddr,
}

impl<'a> CephPrimitiveVersioned<'a> for Mmonjoin<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		fsid: parse_fsid ~
		name: parse_str ~
		addr: call!(EntityAddr::read_from_wire, head_version, compat_version),
		||{
			Mmonjoin{
			fsid: fsid,
			name: name,
			addr: addr,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_monhealth() {
    let bytes = vec![
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 3, 1, 82, 0, 0, 0, 0, 224, 246,
        239, 1, 0, 0, 0, 0, 64, 111, 74, 0, 0, 0, 0, 0, 16, 238, 138, 1, 0, 0, 0, 79, 0, 0, 0, 87,
        213, 143, 86, 208, 99, 210, 18, 1, 1, 40, 0, 0, 0, 139, 244, 154, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
    ];
    let x: &[u8] = &[];
    let expected_result = MonHealth { service_type: 0, service_op: 0,
        data_stats: DataStats {
            fs_stats: ceph_data_stats {
                byte_total: 4294967296,
                byte_used: 23082060486803457,
                byte_avail: 545318575800320,
                avail_percent: 1073741824 },
            last_update: Utime { tv_sec: 19055, tv_nsec: 268435456 },
            store_stats: LevelDBStoreStats {
                bytes_total: 22236523160242926,
                bytes_sst: 7192343780670242816,
                bytes_log: 171815539410,
                bytes_misc: 10155147,
                last_update: Utime { tv_sec: 0, tv_nsec: 0 }
            }
        }
    };
    let result = MonHealth::read_from_wire(&bytes, 0, 0);
    println!("MonHealth: {:?}", result);
    assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_mmonhealth() {
    //let bytes = vec![];
    //let result = Mmonhealth::write_to_wire();
    //println!("ceph_write_Mmonhealth{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct ceph_data_stats {
    pub byte_total: u64,
    pub byte_used: u64,
    pub byte_avail: u64,
    pub avail_percent: i32,
}

impl<'a> CephPrimitiveVersioned<'a> for ceph_data_stats {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
          byte_total: le_u64 ~
          byte_used: le_u64 ~
          byte_avail: le_u64 ~
          avail_percent: le_i32,
		||{
			ceph_data_stats{
                byte_total: byte_total,
                byte_used: byte_used,
                byte_avail: byte_avail,
                avail_percent: avail_percent,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct DataStats {
    pub fs_stats: ceph_data_stats,
    // data dir
    pub last_update: Utime,
    pub store_stats: LevelDBStoreStats,
}
impl<'a> CephPrimitiveVersioned<'a> for DataStats {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
        fs_stats: call!(ceph_data_stats::read_from_wire, head_version, compat_version) ~
        last_update: call!(Utime::read_from_wire, head_version, compat_version) ~
        store_stats: call!(LevelDBStoreStats::read_from_wire, head_version, compat_version),
		||{
			DataStats{
                fs_stats: fs_stats,
                last_update: last_update,
                store_stats: store_stats,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct LevelDBStoreStats {
    pub bytes_total: u64,
    pub bytes_sst: u64,
    pub bytes_log: u64,
    pub bytes_misc: u64,
    pub last_update: Utime,
}

impl<'a> CephPrimitiveVersioned<'a> for LevelDBStoreStats {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
          bytes_total: le_u64 ~
          bytes_sst: le_u64 ~
          bytes_log: le_u64 ~
          bytes_misc: le_u64 ~
          last_update: call!(Utime::read_from_wire, head_version, compat_version),
		||{
			LevelDBStoreStats{
              bytes_total: bytes_total,
              bytes_sst: bytes_sst,
              bytes_log: bytes_log,
              bytes_misc: bytes_misc,
              last_update: last_update,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct MonHealth {
    pub service_type: i32,
    pub service_op: i32,
    pub data_stats: DataStats,
}

impl<'a> CephPrimitiveVersioned<'a> for MonHealth {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        let head_version = 1;
        chain!(input,
		service_type: le_i32 ~
		service_op: le_i32 ~
		data_stats: call!(DataStats::read_from_wire, head_version, compat_version),
		||{
			MonHealth{
			service_type: service_type,
			service_op: service_op,
			data_stats: data_stats,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}

#[test]
fn test_ceph_read_mmonsync() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mmonsync::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_mmonsync() {
    //let bytes = vec![];
    //let result = Mmonsync::write_to_wire();
    //println!("ceph_write_Mmonsync{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct MMonSync<'a> {
    pub op: u32,
    pub cookie: u64,
    pub last_committed: u64,
    pub last_key: (&'a str, &'a str),
    pub chunk_bl: &'a [u8],
    pub reply_to: EntityInstT,
}

impl<'a> CephPrimitiveVersioned<'a> for MMonSync<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		op: le_u32 ~
		cookie: le_u64 ~
		last_committed: le_u64 ~
		last_key: pair!(parse_str,parse_str) ~
        chunk_size: le_u32 ~
		chunk_bl: take!(chunk_size) ~
		reply_to: call!(EntityInstT::read_from_wire, head_version, compat_version),
		||{
			MMonSync{
			op: op,
			cookie: cookie,
			last_committed: last_committed,
			last_key: last_key,
			chunk_bl: chunk_bl,
			reply_to: reply_to,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_mmonpaxos() {
    let bytes = vec![50, 0, 0, 0, 6, 0, 0, 0, 27, 30, 3, 0, 0, 0, 0, 0, 139, 32, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 191, 11, 160, 86, 40, 229, 222, 34, 186, 11, 160, 86, 56, 131, 224, 34, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let x: &[u8] = &[];
    let latest_value: &[u8] = &[];
    let expected_result = MMonPaxos {
        epoch: 50,
        op: PaxosOperation::Lease,
        first_committed: 204315,
        last_committed: 204939,
        pn_from: 0,
        pn: 0,
        uncommitted_pn: 0,
        lease_timestamp: Utime { tv_sec: 1453329343, tv_nsec: 585033000 },
        sent_timestamp: Utime { tv_sec: 1453329338, tv_nsec: 585139000 },
        latest_version: 0,
        latest_value: latest_value,
        values: vec![]
    };
    let result = MMonPaxos::read_from_wire(&bytes, 0, 0);
    println!("MMonPaxos: {:?}", result);
    assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_mmonpaxos() {
    //let bytes = vec![];
    //let result = Mmonpaxos::write_to_wire();
    //println!("ceph_write_Mmonpaxos{:?}", result);
    // assert_eq!(result, expected_bytes);
}

enum_from_primitive!{
#[repr(i32)]
#[derive(RustcEncodable,Debug, Clone, Eq, PartialEq)]
pub enum PaxosOperation {
     Collect =   1, // proposer: propose round
     Last =      2, // voter:    accept proposed round
     Begin =     3, // proposer: value proposed for this round
     Accept =    4, // voter:    accept propsed value
     Commit =    5, // proposer: notify learners of agreed value
     Lease =     6, // leader: extend peon lease
     LeaseAck = 7, // peon: lease ack
}
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct MMonPaxos<'a> {
    pub epoch: u32,
    pub op: PaxosOperation,
    pub first_committed: u64, // i've committed to
    pub last_committed: u64, // i've committed to
    pub pn_from: u64, // i promise to accept after
    pub pn: u64, // with with proposal
    pub uncommitted_pn: u64, // previous pn, if we are a LAST with an uncommitted value
    pub lease_timestamp: Utime,
    pub sent_timestamp: Utime,
    pub latest_version: u64,
    pub latest_value: &'a [u8],
    pub values: Vec<(u64, &'a str)>,
}

impl<'a> CephPrimitiveVersioned<'a> for MMonPaxos<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        let head_version = 3;
        let compat_version = 3;
        chain!(input,
		epoch: le_u32 ~
		op_bits: le_i32 ~
        op: expr_opt!(PaxosOperation::from_i32(op_bits)) ~
		first_committed: le_u64 ~
		last_committed: le_u64 ~
		pn_from: le_u64 ~
		pn: le_u64 ~
		uncommitted_pn: le_u64 ~
		lease_timestamp: call!(Utime::read_from_wire, head_version, compat_version) ~
		sent_timestamp: call!(Utime::read_from_wire, head_version, compat_version) ~
		latest_version: le_u64 ~
        latest_value_size: le_u32 ~
		latest_value: take!(latest_value_size) ~
		count: le_u32 ~
		values: count!(pair!(le_u64, parse_opaque), count as usize),
		||{
			MMonPaxos{
			epoch: epoch,
			op: op,
			first_committed: first_committed,
			last_committed: last_committed,
			pn_from: pn_from,
			pn: pn,
			uncommitted_pn: uncommitted_pn,
			lease_timestamp: lease_timestamp,
			sent_timestamp: sent_timestamp,
			latest_version: latest_version,
			latest_value: latest_value,
			values: vec![],
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}

//
// #[test]
// fn test_ceph_read_MClientQuota(){
// //let bytes = vec![
// TODO: fill in test data here
// ];
// let x: &[u8] = &[];
// let expected_result = "";
// //let result = Mclientquota::read_from_wire(&bytes);
// //println!("ceph_connect_reply: {:?}", result);
// //assert_eq!(Done(x, expected_result), result);
// }
//
// #[test]
// fn test_ceph_write_Mclientquota(){
// //let bytes = vec![
// TODO: fill in result data here
// ];
// //let result = Mclientquota::write_to_wire();
// //println!("ceph_write_Mclientquota{:?}", result);
// assert_eq!(result, expected_bytes);
// }
//
// #[derive(RustcEncodable,Debug,Eq,PartialEq)]
// pub struct Mclientquota{
// pub ino: ino,
// pub rstat: rstat,
// pub quota: quota,
// }
//
// impl<'a> CephPrimitiveVersioned<'a> for Mclientquota{
// fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self>{
// chain!(input,
// ino: call!(inodeno_t::read_from_wire) ~
// rstat: call!(nest_info_t::read_from_wire) ~
// quota: call!(quota_info_t::read_from_wire),
// ||{
// Mclientquota{
// ino: ino,
// rstat: rstat,
// quota: quota,
// }
// })
// }
// fn write_to_wire(&self) -> Result<Vec<u8>, SerialError>{
// let mut buffer: Vec<u8> = Vec::new();
// return Ok(buffer);
// }
// }
//
#[test]
fn test_ceph_read_MAuth() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = Mauth::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_Mauth() {
    //let bytes = vec![];
    //let result = Mauth::write_to_wire();
    //println!("ceph_write_Mauth{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mauth<'a> {
    pub protocol: u32,
    pub auth_payload: &'a [u8],
    pub monmap_epoch: u32,
}

impl<'a> CephPrimitiveVersioned<'a> for Mauth<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		protocol: le_u32 ~
        auth_size: le_u32 ~
		auth_payload: take!(auth_size) ~
		monmap_epoch: le_u32,
		||{
			Mauth{
			protocol: protocol,
			auth_payload: auth_payload,
			monmap_epoch: monmap_epoch,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_MLogAck() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    // let expected_result = Mlogack {
    // };
    //let result = Mlogack::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    ////assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_Mlogack() {
    //let bytes = vec![];
    //let result = Mlogack::write_to_wire();
    //println!("ceph_write_Mlogack{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct Mlogack<'a> {
    pub fsid: Uuid,
    pub last: u64,
    pub channel: &'a str,
}

impl<'a> CephPrimitiveVersioned<'a> for Mlogack<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		fsid: parse_fsid ~
		last: le_u64 ~
		channel: parse_str,
		||{
			Mlogack{
			fsid: fsid,
			last: last,
			channel: channel,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}

#[test]
fn test_ceph_read_ceph_mon_subscribe_item_old() {
    //let bytes = vec![];
    let x: &[u8] = &[];
    let expected_result = "";
    //let result = CephMonSubscribeItemOld::read_from_wire(&bytes);
    //println!("ceph_connect_reply: {:?}", result);
    //assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_CephMonSubscribeItemOld() {
    //let bytes = vec![];
    //let result = CephMonSubscribeItemOld::write_to_wire();
    //println!("ceph_write_CephMonSubscribeItemOld{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct CephMonSubscribeItemOld {
    pub unused: u64,
    pub have: u64,
    pub onetime: u8,
}

impl<'a> CephPrimitiveVersioned<'a> for CephMonSubscribeItemOld {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        chain!(input,
		unused: le_u64 ~
		have: le_u64 ~
		onetime: le_u8,
		||{
			CephMonSubscribeItemOld{
			unused: unused,
			have: have,
			onetime: onetime,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
#[test]
fn test_ceph_read_mroute() {
    let bytes = vec![62, 31, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0];
    let x: &[u8] = &[];
    let expected_result = MRoute {
        session_mon_tid: 7998,
        msg: "",
        dest: EntityInstT {
            name: EntityNameT { _type: 0, _num: 0 },
            addr: EntityAddr { port: 0, nonce: 0, addr: Addr::V4addr(Ipv4Addr::new(0,0,0,0))}
        }
    };
    let result = MRoute::read_from_wire(&bytes, 0, 0);
    println!("MRoute: {:?}", result);
    ////assert_eq!(Done(x, expected_result), result);
}

#[test]
fn test_ceph_write_mroute() {
    //let bytes = vec![];
    //let result = Mroute::write_to_wire();
    //println!("ceph_write_Mroute{:?}", result);
    // assert_eq!(result, expected_bytes);
}

#[derive(RustcEncodable,Debug,Eq,PartialEq)]
pub struct MRoute<'a> {
    pub session_mon_tid: u64,
    pub msg: &'a str,
    pub dest: EntityInstT,
}

impl<'a> CephPrimitiveVersioned<'a> for MRoute<'a> {
    fn read_from_wire(input: &'a [u8], head_version: u16, compat_version: u16) -> nom::IResult<&[u8], Self> {
        let head_version = 1;
        let compat_version = 1;
        chain!(input,
		session_mon_tid: le_u64 ~
		dest: call!(EntityInstT::read_from_wire, head_version, compat_version)~
        m: le_u8~
		msg: parse_str ,
		||{
			MRoute{
			session_mon_tid: session_mon_tid,
			msg: msg,
			dest: dest,
		}
	})
    }
    fn write_to_wire(&self) -> Result<Vec<u8>, SerialError> {
        let mut buffer: Vec<u8> = Vec::new();
        return Ok(buffer);
    }
}
