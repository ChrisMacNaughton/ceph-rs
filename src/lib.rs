#![recursion_limit="100"]

// TODO: Fix this crap
#![allow(dead_code)]

#[macro_use]
extern crate log;
extern crate unix_socket;
extern crate regex;
extern crate rustc_serialize;
#[macro_use] extern crate enum_primitive;
#[macro_use] extern crate bitflags;
#[macro_use] extern crate nom;
extern crate byteorder;
extern crate num;
extern crate pcap;
extern crate simple_logger;
extern crate time;
extern crate users;

mod socket;
pub mod osd;
pub mod mon;
pub mod sniffer;

pub use osd::{
    get_perf_dump_raw as get_osd_perf_dump_raw,
    get_mount,
    mount_point as osd_mount_point,
};

pub use mon::{
    get_perf_dump_raw as get_monitor_perf_dump_raw,
    get_status_raw as get_monitor_health_raw,
    get_quorum_status_raw,
};

#[cfg(test)]
mod tests{
    #[test]
    fn test_mon_perf_dump_raw() {
        let perf = super::get_monitor_perf_dump_raw().unwrap();
    }

    #[test]
    fn test_parse_mount() {
        let mounts = super::get_mount().unwrap();

        assert_eq!(mounts[0], ("/dev/xvda1".to_string(), "/".to_string()));
    }

    #[test]
    fn test_mount_point() {
        let osd_id: u64 = 2;
        let mount = super::osd_mount_point(&osd_id).unwrap();

        assert_eq!(mount, "/dev/xvdf1".to_string());
    }
}
