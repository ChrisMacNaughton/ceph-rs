use socket;
use std::process::Command;
use std::io;

macro_rules! send (
    ($msg:expr, $osd_num:ident) => {
        match send($osd_num, $msg){
            Ok(s) => Some(s),
            Err(e) => {
                info!("Had an error getting $msg: {:?}", e);
                return None;
            }
        }
    }
);


#[cfg(test)]
mod tests {
    #[test]
    fn test_parse_osd() {
        let osd_id: u64 = 2;
        let result = super::has_osd(&osd_id, ("/dev/xvdf1".to_string(), "/var/lib/ceph/osd/ceph-2".to_string()));
        assert!(result);
    }
}

pub fn get_perf_dump_raw(osd_num: &u64) -> Option<String> {
    send!("perf dump", osd_num)
}

fn send(osd_num: &u64, command: &str) -> Result<String, String>{
    let sock_path = format!("/var/run/ceph/ceph-osd.{}.asok", osd_num);

    socket::send(sock_path, command)
}

pub fn mount_point(osd_num: &u64) -> Option<String> {
    let mounts = match get_mount() {
        Ok(s) => s,
        Err(_) => return None,
    };
    let mut iter = mounts.into_iter();
    while let Some(x) = iter.next() {
        if has_osd(osd_num, x.clone()) {
            return Some(x.0);
        }
    }
    None
}

fn has_osd(osd_num: &u64, mount: (String, String)) -> bool {
    let (_, path): (String, String) = mount;
    // let path_str: &str = path.as_ref();
    let path_string= format!("osd/ceph-{}", osd_num);
    let path_name: &str = path_string.as_ref();
    // println!("Checking if {:?} contains {:?}", path, path_name);
    path.contains(path_name)
    // false
}

pub fn get_mount() -> Result<Vec<(String, String)>, io::Error> {
    let mount_res = if cfg!(test) {
        "/dev/xvda1 on / type ext4 (rw,discard)
proc on /proc type proc (rw,noexec,nosuid,nodev)
sysfs on /sys type sysfs (rw,noexec,nosuid,nodev)
none on /sys/fs/cgroup type tmpfs (rw)
none on /sys/fs/fuse/connections type fusectl (rw)
none on /sys/kernel/debug type debugfs (rw)
none on /sys/kernel/security type securityfs (rw)
udev on /dev type devtmpfs (rw,mode=0755)
devpts on /dev/pts type devpts (rw,noexec,nosuid,gid=5,mode=0620)
tmpfs on /run type tmpfs (rw,noexec,nosuid,size=10%,mode=0755)
none on /run/lock type tmpfs (rw,noexec,nosuid,nodev,size=5242880)
none on /run/shm type tmpfs (rw,nosuid,nodev)
none on /run/user type tmpfs (rw,noexec,nosuid,nodev,size=104857600,mode=0755)
none on /sys/fs/pstore type pstore (rw)
systemd on /sys/fs/cgroup/systemd type cgroup (rw,noexec,nosuid,nodev,none,name=systemd)
/dev/xvdb on /mnt type ext3 (rw,_netdev)
/dev/xvdf1 on /var/lib/ceph/osd/ceph-2 type xfs (rw,noatime,inode64)
".to_string()
    } else {
        let output = try!(Command::new("mount").output());
        match String::from_utf8(output.stdout) {
            Ok(s) => s,
            Err(_) => "".to_string()
        }
    };

    let mounts = mount_res.split("\n")
        .filter(|&x| x != "")
        .map(before_paren)
        .map(remove_type)
        .map(|x| x.to_owned())
        .map(split_parts)
        .collect::<Vec<(String, String)>>();
    // let mount_iter = mounts.into_iter();
    // mount_iter.map(|&x|
    //         x.split('(').collect::<Vec<String>>()[0]
    //     );
    // println!("{:?}", mounts);

    // Ok(vec!(("test".to_string(),"test".to_string())))
    Ok(mounts)
}

fn before_paren(x: &str) -> &str {
    x.split('(').collect::<Vec<&str>>()[0]
}

fn remove_type(x: &str) -> &str {
    x.split("type").collect::<Vec<&str>>()[0]
}

fn split_parts(x: String) -> (String, String) {
    let parts = x.split(" on ").collect::<Vec<&str>>();
    (parts[0].trim().to_string(), parts[1].trim().to_string())
}
