extern crate unix_socket;
use unix_socket::UnixStream;
use std::io::prelude::*;


mod tests {
    #[test]
    fn test_socket_send(){
        let val = super::send("test".to_string(), "value");

        assert_eq!(val, Err("Not Found".to_string()));
    }
}

pub fn send(socket_name: String, command: &str) -> Result<String, String>  {
    if cfg!(test) {
        send_test(socket_name, command)
    } else {
        send_to_socket(socket_name, command)
    }
}


fn send_to_socket(socket_name: String, command: &str) -> Result<String, String> {
    let command_str = format!("{{\"prefix\": \"{}\"}}\0", command);
    let mut stream = try!(UnixStream::connect(socket_name).map_err(|e| e.to_string()));
    let mut output_buf = Vec::new();

    let _ = stream.write(command_str.as_bytes());
    let _ = match stream.read_to_end(&mut output_buf) {
        Ok(s) => s,
        Err(e) => {
            warn!("Got an error from the socket: {:?}\n{:?}", e, output_buf);
            return Err(format!("{:?}", e));
        }
    };

    let mut output_string:String = String::from_utf8_lossy(&output_buf).to_string();
    output_string = output_string.split_whitespace().collect();

    for c in output_string.clone().chars() {
        if c == '{' {
            break;
        }
        output_string.remove(0);
    }

    Ok(output_string)
}

// This method is used in testing
fn send_test(socket: String, command: &str) -> Result<String, String> {
    println!("Testing with command '{}' to socket '{}'", command, socket);
    let res = match socket.as_ref() {
        "/var/run/ceph/ceph-osd.0.asok" => {
            match command {
                "perf dump" => {
                    "{\"WBThrottle\":{\"bytes_dirtied\":0,\"bytes_wb\":0,\"ios_dirtied\":0,\"ios_wb\":0,\"inodes_dirtied\":0,\"inodes_wb\":0},\"filestore\":{\"journal_queue_max_ops\":0,\"journal_queue_ops\":0,\"journal_ops\":1105,\"journal_queue_max_bytes\":0,\"journal_queue_bytes\":0,\"journal_bytes\":1092913,\"journal_latency\":{\"avgcount\":1105,\"sum\":24.425844000},\"journal_wr\":347,\"journal_wr_bytes\":{\"avgcount\":347,\"sum\":4558848},\"journal_full\":0,\"committing\":0,\"commitcycle\":2,\"commitcycle_interval\":{\"avgcount\":2,\"sum\":10.170083000},\"commitcycle_latency\":{\"avgcount\":2,\"sum\":0.160788000},\"op_queue_max_ops\":50,\"op_queue_ops\":0,\"ops\":1105,\"op_queue_max_bytes\":104857600,\"op_queue_bytes\":0,\"bytes\":1086283,\"apply_latency\":{\"avgcount\":1105,\"sum\":218.908710000},\"queue_transaction_latency_avg\":{\"avgcount\":1105,\"sum\":7.837429000}},\"leveldb\":{\"leveldb_get\":1361,\"leveldb_transaction\":1553,\"leveldb_compact\":0,\"leveldb_compact_range\":0,\"leveldb_compact_queue_merge\":0,\"leveldb_compact_queue_len\":0},\"mutex-FileJournal::completions_lock\":{\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"mutex-FileJournal::finisher_lock\":{\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"mutex-FileJournal::write_lock\":{\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"mutex-FileJournal::writeq_lock\":{\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"mutex-JOS::ApplyManager::apply_lock\":{\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"mutex-JOS::ApplyManager::com_lock\":{\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"mutex-JOS::SubmitManager::lock\":{\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"mutex-WBThrottle::lock\":{\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"objecter\":{\"op_active\":0,\"op_laggy\":0,\"op_send\":0,\"op_send_bytes\":0,\"op_resend\":0,\"op_ack\":0,\"op_commit\":0,\"op\":0,\"op_r\":0,\"op_w\":0,\"op_rmw\":0,\"op_pg\":0,\"osdop_stat\":0,\"osdop_create\":0,\"osdop_read\":0,\"osdop_write\":0,\"osdop_writefull\":0,\"osdop_append\":0,\"osdop_zero\":0,\"osdop_truncate\":0,\"osdop_delete\":0,\"osdop_mapext\":0,\"osdop_sparse_read\":0,\"osdop_clonerange\":0,\"osdop_getxattr\":0,\"osdop_setxattr\":0,\"osdop_cmpxattr\":0,\"osdop_rmxattr\":0,\"osdop_resetxattrs\":0,\"osdop_tmap_up\":0,\"osdop_tmap_put\":0,\"osdop_tmap_get\":0,\"osdop_call\":0,\"osdop_watch\":0,\"osdop_notify\":0,\"osdop_src_cmpxattr\":0,\"osdop_pgls\":0,\"osdop_pgls_filter\":0,\"osdop_other\":0,\"linger_active\":0,\"linger_send\":0,\"linger_resend\":0,\"poolop_active\":0,\"poolop_send\":0,\"poolop_resend\":0,\"poolstat_active\":0,\"poolstat_send\":0,\"poolstat_resend\":0,\"statfs_active\":0,\"statfs_send\":0,\"statfs_resend\":0,\"command_active\":0,\"command_send\":0,\"command_resend\":0,\"map_epoch\":9,\"map_full\":0,\"map_inc\":3,\"osd_sessions\":0,\"osd_session_open\":0,\"osd_session_close\":0,\"osd_laggy\":0},\"osd\":{\"opq\":0,\"op_wip\":0,\"op\":0,\"op_in_bytes\":0,\"op_out_bytes\":0,\"op_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"op_process_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"op_r\":0,\"op_r_out_bytes\":0,\"op_r_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"op_r_process_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"op_w\":0,\"op_w_in_bytes\":0,\"op_w_rlat\":{\"avgcount\":0,\"sum\":0.000000000},\"op_w_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"op_w_process_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"op_rw\":0,\"op_rw_in_bytes\":0,\"op_rw_out_bytes\":0,\"op_rw_rlat\":{\"avgcount\":0,\"sum\":0.000000000},\"op_rw_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"op_rw_process_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"subop\":0,\"subop_in_bytes\":0,\"subop_latency\":{\"avgcount\":0,\"sum\":2.801685787},\"subop_w\":0,\"subop_w_in_bytes\":0,\"subop_w_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"subop_pull\":0,\"subop_pull_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"subop_push\":0,\"subop_push_in_bytes\":0,\"subop_push_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"pull\":0,\"push\":0,\"push_out_bytes\":0,\"push_in\":0,\"push_in_bytes\":0,\"recovery_ops\":0,\"loadavg\":71,\"buffer_bytes\":0,\"numpg\":104,\"numpg_primary\":66,\"numpg_replica\":38,\"numpg_stray\":0,\"heartbeat_to_peers\":2,\"heartbeat_from_peers\":0,\"map_messages\":12,\"map_message_epochs\":32,\"map_message_epoch_dups\":21,\"messages_delayed_for_map\":1,\"stat_bytes\":8320901120,\"stat_bytes_used\":2360205312,\"stat_bytes_avail\":5514424320,\"copyfrom\":0,\"tier_promote\":0,\"tier_flush\":0,\"tier_flush_fail\":0,\"tier_try_flush\":0,\"tier_try_flush_fail\":0,\"tier_evict\":0,\"tier_whiteout\":0,\"tier_dirty\":0,\"tier_clean\":0,\"tier_delay\":0,\"agent_wake\":0,\"agent_skip\":0,\"agent_flush\":0,\"agent_evict\":0},\"recoverystate_perf\":{\"initial_latency\":{\"avgcount\":192,\"sum\":0.025111000},\"started_latency\":{\"avgcount\":111,\"sum\":127.410766000},\"reset_latency\":{\"avgcount\":303,\"sum\":0.074083000},\"start_latency\":{\"avgcount\":303,\"sum\":0.035167000},\"primary_latency\":{\"avgcount\":94,\"sum\":120.398240000},\"peering_latency\":{\"avgcount\":160,\"sum\":101.574593000},\"backfilling_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"waitremotebackfillreserved_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"waitlocalbackfillreserved_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"notbackfilling_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"repnotrecovering_latency\":{\"avgcount\":10,\"sum\":1.676035000},\"repwaitrecoveryreserved_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"repwaitbackfillreserved_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"RepRecovering_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"activating_latency\":{\"avgcount\":153,\"sum\":190.964855000},\"waitlocalrecoveryreserved_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"waitremoterecoveryreserved_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"recovering_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"recovered_latency\":{\"avgcount\":66,\"sum\":0.001166000},\"clean_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"active_latency\":{\"avgcount\":87,\"sum\":67.597389000},\"replicaactive_latency\":{\"avgcount\":10,\"sum\":1.676212000},\"stray_latency\":{\"avgcount\":143,\"sum\":55.406181000},\"getinfo_latency\":{\"avgcount\":160,\"sum\":26.784473000},\"getlog_latency\":{\"avgcount\":160,\"sum\":0.007970000},\"waitactingchange_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"incomplete_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"getmissing_latency\":{\"avgcount\":160,\"sum\":0.006214000},\"waitupthru_latency\":{\"avgcount\":125,\"sum\":74.765738000}},\"throttle-filestore_bytes\":{\"val\":0,\"max\":0,\"get\":0,\"get_sum\":0,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":0,\"put_sum\":0,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-filestore_ops\":{\"val\":0,\"max\":0,\"get\":0,\"get_sum\":0,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":0,\"put_sum\":0,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-msgr_dispatch_throttler-client\":{\"val\":0,\"max\":104857600,\"get\":32,\"get_sum\":62721,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":32,\"put_sum\":62721,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-msgr_dispatch_throttler-cluster\":{\"val\":0,\"max\":104857600,\"get\":1242,\"get_sum\":1399368,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":1242,\"put_sum\":1399368,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-msgr_dispatch_throttler-hb_back_server\":{\"val\":0,\"max\":104857600,\"get\":29,\"get_sum\":1363,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":29,\"put_sum\":1363,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-msgr_dispatch_throttler-hb_front_server\":{\"val\":0,\"max\":104857600,\"get\":29,\"get_sum\":1363,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":29,\"put_sum\":1363,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-msgr_dispatch_throttler-hbclient\":{\"val\":0,\"max\":104857600,\"get\":68,\"get_sum\":3196,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":68,\"put_sum\":3196,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-msgr_dispatch_throttler-ms_objecter\":{\"val\":0,\"max\":104857600,\"get\":0,\"get_sum\":0,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":0,\"put_sum\":0,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-objecter_bytes\":{\"val\":0,\"max\":104857600,\"get\":0,\"get_sum\":0,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":0,\"put_sum\":0,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-objecter_ops\":{\"val\":0,\"max\":1024,\"get\":0,\"get_sum\":0,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":0,\"put_sum\":0,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-osd_client_bytes\":{\"val\":0,\"max\":524288000,\"get\":0,\"get_sum\":0,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":0,\"put_sum\":0,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-osd_client_messages\":{\"val\":0,\"max\":100,\"get\":0,\"get_sum\":0,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":0,\"put_sum\":0,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}}}"
                },
                _ => return Err("Not Found".to_string()),
            }
        },
        "/var/run/ceph/ceph-mon.ip-172-31-62-169.asok" => {
            match command {
                "perf dump" => {
                    "{\"cluster\":{\"num_mon\":3,\"num_mon_quorum\":3,\"num_osd\":3,\"num_osd_up\":3,\"num_osd_in\":3,\"osd_epoch\":9,\"osd_kb\":24377640,\"osd_kb_used\":7161420,\"osd_kb_avail\":15908784,\"num_pool\":3,\"num_pg\":192,\"num_pg_active_clean\":192,\"num_pg_active\":192,\"num_pg_peering\":0,\"num_object\":0,\"num_object_degraded\":0,\"num_object_unfound\":0,\"num_bytes\":0,\"num_mds_up\":0,\"num_mds_in\":0,\"num_mds_failed\":0,\"mds_epoch\":1},\"leveldb\":{\"leveldb_get\":84674,\"leveldb_transaction\":6515,\"leveldb_compact\":0,\"leveldb_compact_range\":18,\"leveldb_compact_queue_merge\":0,\"leveldb_compact_queue_len\":0},\"mon\":{},\"paxos\":{\"start_leader\":0,\"start_peon\":5,\"restart\":22,\"refresh\":3244,\"refresh_latency\":{\"avgcount\":3244,\"sum\":0.727879000},\"begin\":3244,\"begin_keys\":{\"avgcount\":0,\"sum\":0},\"begin_bytes\":{\"avgcount\":3244,\"sum\":23142256},\"begin_latency\":{\"avgcount\":3244,\"sum\":26.417556000},\"commit\":3244,\"commit_keys\":{\"avgcount\":0,\"sum\":0},\"commit_bytes\":{\"avgcount\":0,\"sum\":0},\"commit_latency\":{\"avgcount\":0,\"sum\":0.000000000},\"collect\":5,\"collect_keys\":{\"avgcount\":5,\"sum\":5},\"collect_bytes\":{\"avgcount\":5,\"sum\":120},\"collect_latency\":{\"avgcount\":5,\"sum\":0.024616000},\"collect_uncommitted\":0,\"collect_timeout\":0,\"accept_timeout\":0,\"lease_ack_timeout\":0,\"lease_timeout\":1,\"store_state\":3244,\"store_state_keys\":{\"avgcount\":3244,\"sum\":31743},\"store_state_bytes\":{\"avgcount\":3244,\"sum\":45386225},\"store_state_latency\":{\"avgcount\":3244,\"sum\":22.301581000},\"share_state\":1,\"share_state_keys\":{\"avgcount\":1,\"sum\":8},\"share_state_bytes\":{\"avgcount\":1,\"sum\":59057},\"new_pn\":0,\"new_pn_latency\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-mon_client_bytes\":{\"val\":0,\"max\":104857600,\"get\":50,\"get_sum\":3938,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":50,\"put_sum\":3938,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-mon_daemon_bytes\":{\"val\":0,\"max\":419430400,\"get\":1010,\"get_sum\":145454,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":1010,\"put_sum\":145454,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}},\"throttle-msgr_dispatch_throttler-mon\":{\"val\":0,\"max\":104857600,\"get\":33888,\"get_sum\":49238759,\"get_or_fail_fail\":0,\"get_or_fail_success\":0,\"take\":0,\"take_sum\":0,\"put\":33888,\"put_sum\":49238759,\"wait\":{\"avgcount\":0,\"sum\":0.000000000}}}"
                },
                "mon_status" => {
                    "{\"name\":\"ip-172-31-62-169\",\"rank\":2,\"state\":\"peon\",\"election_epoch\":8,\"quorum\":[0,1,2],\"outside_quorum\":[],\"extra_probe_peers\":[],\"sync_provider\":[],\"monmap\":{\"epoch\":1,\"fsid\":\"1bb15abc-4158-11e5-b499-00151737cf98\",\"modified\":\"0.000000\",\"created\":\"0.000000\",\"mons\":[{\"rank\":0,\"name\":\"ip-172-31-10-69\",\"addr\":\"172.31.10.69:6789\\/0\"},{\"rank\":1,\"name\":\"ip-172-31-26-27\",\"addr\":\"172.31.26.27:6789\\/0\"},{\"rank\":2,\"name\":\"ip-172-31-62-169\",\"addr\":\"172.31.62.169:6789\\/0\"}]}}"
                },
                "quorum_status" => {
                    "{\"election_epoch\":6,\"quorum\":[0,1,2],\"quorum_names\":[\"ip-172-31-3-220\",\"ip-172-31-18-107\",\"ip-172-31-52-57\"],\"quorum_leader_name\":\"ip-172-31-3-220\",\"monmap\":{\"epoch\":2,\"fsid\":\"1bb15abc-4158-11e5-b499-00151737cf98\",\"modified\":\"2015-11-0313:06:48.373343\",\"created\":\"0.000000\",\"mons\":[{\"rank\":0,\"name\":\"ip-172-31-3-220\",\"addr\":\"172.31.3.220:6789\\/0\"},{\"rank\":1,\"name\":\"ip-172-31-18-107\",\"addr\":\"172.31.18.107:6789\\/0\"},{\"rank\":2,\"name\":\"ip-172-31-52-57\",\"addr\":\"172.31.52.57:6789\\/0\"}]}}"
                }
                _ => return Err("Not Found".to_string()),
            }
        },
        _ => return Err("Not Found".to_string()),
    };
    Ok(res.to_string())
}
