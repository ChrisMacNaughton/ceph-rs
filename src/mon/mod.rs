use std::path::Path;
use std::fs;
// use std::io::prelude::*;

use socket;
use regex::Regex;

macro_rules! send (
    ($msg:expr) => {
        match send($msg){
            Ok(s) => Some(s),
            Err(e) => {
                info!("Had an error getting $msg: {:?}", e);
                return None;
            }
        }
    }
);

pub fn get_perf_dump_raw() -> Option<String> {
    send!("perf dump")
}

pub fn get_status_raw() -> Option<String> {
    send!("mon_status")
}

pub fn get_quorum_status_raw() -> Option<String>{
    send!("quorum_status")
}

fn send(command: &str) -> Result<String, String>{
    let sock_path = match mon_socket() {
        Ok(p) => format!("/var/run/ceph/{}", p),
        Err(e) => {
            info!("Had an error: {:?}", e);
            return Err("Not a monitor".to_string());
        }
    };
    trace!("Sending {} to {}", command, sock_path);
    socket::send(sock_path, command)
}

fn mon_socket() -> Result<String, String> {
    if cfg!(test) {
        Ok("ceph-mon.ip-172-31-62-169.asok".to_string())
    } else {
        for entry in try!(fs::read_dir(Path::new("/var/run/ceph")).map_err(|e| e.to_string())){
            let entry = try!(entry.map_err(|e| e.to_string()));
            let sock_addr_osstr = entry.file_name();
            let file_name = match sock_addr_osstr.to_str(){
                Some(name) => name,
                None => {
                    //Skip files we can't turn into a string
                    continue;
                }
            };
            if is_a_mon(file_name) {
                return Ok(file_name.to_string());
            }
        }
        Err("Not a Mon".to_string())
    }
}

fn is_a_mon(file_name: &str) -> bool {
    let mon_regex = Regex::new(r"ceph-mon..+.asok").unwrap();

    match mon_regex.captures(file_name){
        Some(_) => {
            true
        },
        None => false
    }
}

#[cfg(test)]
#[test]
fn test_is_a_mon() {
    // assert_eq!(true, true)
    assert!(is_a_mon("ceph-mon.ip-172-31-62-169.asok"));
    assert_eq!(is_a_mon("ceph-osd.1.asok"), false);
}
