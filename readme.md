# Ceph

[![Build Status](https://travis-ci.org/ChrisMacNaughton/ceph-rs.svg?branch=master)](https://travis-ci.org/ChrisMacNaughton/ceph-rs)

Ceph is a crate to talk to ceph monitors / OSDs via their admin socket to get information about cluster state